﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The bread object. Sequentially transitions between stages of eatedness. 
/// </summary>
public class Bread : MonoBehaviour {
    public enum BreadState {
        Full,
        Bites1,
        Bites2,
        Bites3,
        Bites4,
        Bites5,
        Empty
    }

    public Sprite bread0;
    public Sprite bread1;
    public Sprite bread2;
    public Sprite bread3;
    public Sprite bread4;
    public Sprite bread5;
    public Sprite bread6;

    public MainController mainController;

    // Amount of time between each bread state transition while chewing
    public float ChewLatency = 1f;
    // Minimum amount of time to wait before bread should respawn
    public float BreadRespawnMinTime = 1f;
    // How long to wait in between each check to see if the bread should respawn
    public float BreadRespawnCheckTime = .75f;

    private BreadState currentState = BreadState.Full;
    private SpriteRenderer spriteRenderer;

    private bool isChewing;
    private bool breadStateLocked;
    private bool breadAvailable = true;
    public bool BreadAvailable() {
        return breadAvailable;
    }
    private bool isInNeutralState = true;

    // Start is called before the first frame update
    void Start() {
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    public void TransitionToNextState() {
        switch (currentState) {
            case BreadState.Full:
                currentState = BreadState.Bites1;
                spriteRenderer.sprite = bread1;
                break;
            case BreadState.Bites1:
                currentState = BreadState.Bites2;
                spriteRenderer.sprite = bread2;
                break;
            case BreadState.Bites2:
                currentState = BreadState.Bites3;
                spriteRenderer.sprite = bread3;
                break;
            case BreadState.Bites3:
                currentState = BreadState.Bites4;
                spriteRenderer.sprite = bread4;
                break;
            case BreadState.Bites4:
                currentState = BreadState.Bites5;
                spriteRenderer.sprite = bread5;
                break;
            case BreadState.Bites5:
                currentState = BreadState.Empty;
                spriteRenderer.sprite = bread6;

                StartCoroutine(RespawnBread());

                break;
            case BreadState.Empty:
                break;
        }
    }

    public void StartChewing() {
        isChewing = true;
        StartCoroutine(Chewing());
    }

    public void StopChewing() {
        isChewing = false;
    }

    public void OnToNone() {
        isInNeutralState = true;
    }

    public void OffToNone() {
        isInNeutralState = false;
    }

    private IEnumerator Chewing() {
        // TODO: Make this reset completely when exiting the chewing state
        while (isChewing) {
            yield return new WaitForSeconds(ChewLatency);
            if (isChewing)
                TransitionToNextState();
        }
    }

    private IEnumerator RespawnBread() {
        breadAvailable = false;

        // Give it a bit of time so animations and sounds can finish
        yield return new WaitForSeconds(.5f);

        mainController.HandleBreadUnavailable();

        yield return new WaitForSeconds(BreadRespawnMinTime);

        while (!isInNeutralState)
            yield return new WaitForSeconds(BreadRespawnCheckTime);

        currentState = BreadState.Full;
        spriteRenderer.sprite = bread0;
        breadAvailable = true;
        mainController.HandleBreadAvailable();
    }
}
