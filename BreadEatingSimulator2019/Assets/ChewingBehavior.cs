﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChewingBehavior : StateMachineBehaviour {
    // Amount of time in seconds to wait after starting chew animation until starting chew sounds
    private const float chewDelayTimerLength = .15f;

    //private bool isInState = false;
    private MainController mainController => MainController.GetMainController();

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (RandomUtil.RandomBool())
            mainController.audioManager.PlayChewRegularSounds();
        else
            mainController.audioManager.PlayChewBetterSounds();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        //isInState = false;
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
