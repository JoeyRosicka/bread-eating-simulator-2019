﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles storing, playing, and stopping sounds
/// </summary>
public class AudioManager : MonoBehaviour {
    public AudioSource PrepBeginning;
    public AudioSource Prep;
    public AudioSource ChewRegularBeginning;
    public AudioSource ChewRegular;
    public AudioSource ChewBetterBeginning;
    public AudioSource ChewBetter;
    public AudioSource EatUpwards;
    public AudioSource EatAggressive;
    public AudioSource EatConfident;
    public AudioSource EatMinimalist;
    public AudioSource EatExtra;
    public AudioSource BreadRespawn;

    // Sound that's currently playing. Only one at a time, lads.
    private AudioSource currentlyPlayingSound;
    private AudioSource queuedSound;
    private bool repeatQueuedSound;
    private bool soundsToggled = true;

    // Amount of time (in seconds) that can be left in the currently playing sound clip before we can switch to the queued sound clip
    private const float transitionHeadStart = .02f;

    private void PlaySound(AudioSource sound, bool repeat) {
        if (!soundsToggled)
            return;

        if (currentlyPlayingSound)
            currentlyPlayingSound.Stop();

        currentlyPlayingSound = sound;
        sound.loop = repeat;
        sound.Play();
    }

    private void QueueSound(AudioSource sound, bool repeat) {
        if (!currentlyPlayingSound || !currentlyPlayingSound.isPlaying) {
            PlaySound(sound, repeat);
            return;
        }

        queuedSound = sound;
        repeatQueuedSound = repeat;
    }

    private void CheckForSoundFinished() {
        if (currentlyPlayingSound && currentlyPlayingSound.isPlaying && currentlyPlayingSound.clip.length - currentlyPlayingSound.time > transitionHeadStart) return;
        if (queuedSound) {
            currentlyPlayingSound = queuedSound;
            queuedSound = null;
            PlaySound(currentlyPlayingSound, repeatQueuedSound);
        }
    }

    private void Update() {
        CheckForSoundFinished();
    }

    public void StopSounds() {
        if (currentlyPlayingSound)
            currentlyPlayingSound.Stop();
        currentlyPlayingSound = null;
        queuedSound = null;
    }

    public void ToggleSounds(bool toggled) {
        soundsToggled = toggled;
    }

    public void PlayPrepSounds() {
        PlaySound(PrepBeginning, false);
        QueueSound(Prep, true);
    }

    public void PlayEatUpwardsSound() {
        PlaySound(EatUpwards, false);
    }

    public void PlayEatAggressiveSound() {
        PlaySound(EatAggressive, false);
    }

    public void PlayEatConfidentSound() {
        PlaySound(EatConfident, false);
    }

    public void PlayEatMinimalistSound() {
        PlaySound(EatMinimalist, false);
    }

    public void PlayEatExtraSound() {
        PlaySound(EatExtra, false);
    }

    public void PlayChewRegularSounds() {
        PlaySound(ChewRegularBeginning, false);
        QueueSound(ChewRegular, true);
    }

    public void PlayChewBetterSounds() {
        PlaySound(ChewBetterBeginning, false);
        QueueSound(ChewBetter, true);
    }

    public void PlayBreadRespawnSound() {
        PlaySound(BreadRespawn, false);
    }
}
