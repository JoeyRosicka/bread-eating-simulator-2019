﻿using UnityEngine;

/// <summary>
/// Controls movement and pivoting of the arms (and the bread being held)
/// </summary>
public class ArmMover : MonoBehaviour {
    public enum MoveDirection {
        Up,
        None,
        Down
    }

    public MainController Controller;

    [Range(0, 1)]
    public float noMoveThreshold;
    [Range(0, 1)]
    public float previousMoveThreshold;

    private float previousMoveVertical = 0;

    /// <summary>
    /// Detect movements and update animation triggers
    /// </summary>
    void FixedUpdate() {
        float moveVertical = Input.GetAxis("Vertical");
        MoveDirection direction = GetDirection(moveVertical);
        float speed = GetSpeed(direction, moveVertical);
        Controller.UpdateAnimations(direction, speed);
    }

    private MoveDirection GetDirection(float moveVertical) {
        MoveDirection nextDirection;
        if (moveVertical > noMoveThreshold) {
            if (moveVertical >= previousMoveVertical - previousMoveThreshold)
                nextDirection = MoveDirection.Up;
            else
                nextDirection = MoveDirection.None;
        } else if (moveVertical < -noMoveThreshold) {
            if (moveVertical <= previousMoveVertical + previousMoveThreshold)
                nextDirection = MoveDirection.Down;
            else
                nextDirection = MoveDirection.None;
        } else {
            nextDirection = MoveDirection.None;
        }

        previousMoveVertical = moveVertical;
        return nextDirection;
    }

    private float GetSpeed(MoveDirection direction, float speed) {
        if (direction == MoveDirection.None)
            return 1;
        return Mathf.Abs(speed);
    }
}
