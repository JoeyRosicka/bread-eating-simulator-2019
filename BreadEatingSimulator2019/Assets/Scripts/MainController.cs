﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static ArmMover;

/// <summary>
/// Controls everything
/// </summary>
public class MainController : MonoBehaviour {
     /// <summary>
     /// State of eating, controlled by the arms. 
     /// </summary>
    public enum EatingPositionState {
        None,       // 0
        Down,       // 1
        Up          // 2
    }

    public enum EatingPositionTransition {
        DownToNone, // 0
        UpToNone,   // 1
        ToDown,     // 2
        ToUp,       // 3
    }

    // Active animators to respond directly to user input (arms, bread, head)
    public List<Animator> InputAnimators;
    public Animator MouthAnimator;
    public Animator HeadAnimator;
    public Animator HairStrandLeftAnimator;
    public Animator HairStrandRightAnimator;

    public AudioManager audioManager;

    public Bread bread;

    /// <summary>
    /// Chance for any given bite to be a single one rather than a long chewing animation
    /// </summary>
    [Range(0, 1)]
    public float chanceForSingleBite;

    public EatingPositionState CurrentState { get;  set; }

    private static MainController mainController;
    public static MainController GetMainController() {
        return mainController;
    }

    private bool keepChewing;

    void Start() {
        CurrentState = EatingPositionState.None;
        mainController = this;
    }

    void Update() {
        //InputManager.instance.UpdateVibration();
    }

    public void HandleBreadUnavailable() {
        audioManager.ToggleSounds(false);
        audioManager.StopSounds();
        AnimateBreadRespawning(true);
    }

    public void HandleBreadAvailable() {
        audioManager.ToggleSounds(true);
        audioManager.PlayBreadRespawnSound();
        AnimateBreadRespawning(false);
    }

    public void UpdateAnimations(MoveDirection direction, float animationSpeed) {
        foreach (Animator animator in InputAnimators) {
            if (direction == MoveDirection.Up) {
                animator.SetTrigger("Input Up");
                animator.ResetTrigger("Input None");
                animator.ResetTrigger("Input Down");
            } else if (direction == MoveDirection.None) {
                animator.ResetTrigger("Input Up");
                animator.SetTrigger("Input None");
                animator.ResetTrigger("Input Down");
            } else if (direction == MoveDirection.Down) {
                animator.ResetTrigger("Input Up");
                animator.ResetTrigger("Input None");
                animator.SetTrigger("Input Down");
            }
            animator.speed = animationSpeed;
        }
    }

    public void TriggerTransition(EatingPositionTransition transition) {
        switch (transition) {
            case EatingPositionTransition.DownToNone:
                CurrentState = EatingPositionState.None;
                OnDownToNone();
                break;
            case EatingPositionTransition.UpToNone:
                CurrentState = EatingPositionState.None;
                OnUpToNone();
                break;
            case EatingPositionTransition.ToDown:
                CurrentState = EatingPositionState.Down;
                OnToDown();
                break;
            case EatingPositionTransition.ToUp:
                CurrentState = EatingPositionState.Up;
                OnToUp();
                break;
            default:
                throw new System.Exception("Unidentified eating position transition: " + transition);
        }
    }

    /// <summary>
    /// To support external coroutines being called
    /// </summary>
    public void StartChildCoroutine(IEnumerator coroutineMethod) {
        StartCoroutine(coroutineMethod);
    }

    private void AnimateBreadRespawning(bool isAvailable) {
        string breadRespawning = "Bread Respawning";
        HeadAnimator.SetBool(breadRespawning, isAvailable);
        MouthAnimator.SetBool(breadRespawning, isAvailable);
        HairStrandLeftAnimator.SetBool(breadRespawning, isAvailable);
        HairStrandRightAnimator.SetBool(breadRespawning, isAvailable);
    }

    private void OnDownToNone() {
        Debug.Log("OnDownToNone");
        audioManager.StopSounds();
        bread.OnToNone();
    }

    private void OnUpToNone() {
        Debug.Log("OnUpToNone");
        MouthAnimator.SetInteger("State", (int)EatingPositionState.None);
        HairStrandLeftAnimator.SetInteger("State", (int)EatingPositionState.None);
        HairStrandRightAnimator.SetInteger("State", (int)EatingPositionState.None);
        audioManager.StopSounds();
        bread.StopChewing();
        bread.OnToNone();
    }

    private void OnToDown() {
        Debug.Log("OnToDown");
        audioManager.PlayPrepSounds();
        bread.OffToNone();
    }

    private void OnToUp() {
        Debug.Log("OnToUp");
        MouthAnimator.SetInteger("State", (int)EatingPositionState.Up);
        HairStrandLeftAnimator.SetInteger("State", (int)EatingPositionState.Up);
        HairStrandRightAnimator.SetInteger("State", (int)EatingPositionState.Up);

        keepChewing = Random.Range(0f, 1f) > chanceForSingleBite;
        MouthAnimator.SetBool("Keep Chewing", keepChewing);
        HeadAnimator.SetBool("Keep Chewing", keepChewing);
        HairStrandLeftAnimator.SetBool("Keep Chewing", keepChewing);
        HairStrandRightAnimator.SetBool("Keep Chewing", keepChewing);

        if (keepChewing)
            bread.StartChewing();
        else {
            bread.TransitionToNextState();

            int eatSound = Random.Range(0, 5);
            switch (eatSound) {
                case 0:
                    audioManager.PlayEatAggressiveSound();
                    break;
                case 1:
                    audioManager.PlayEatUpwardsSound();
                    break;
                case 2:
                    audioManager.PlayEatConfidentSound();
                    break;
                case 3:
                    audioManager.PlayEatMinimalistSound();
                    break;
                case 4:
                    audioManager.PlayEatExtraSound();
                    break;
            }
        }

        bread.OffToNone();
    }
}
