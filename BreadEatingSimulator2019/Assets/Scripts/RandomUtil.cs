﻿using UnityEngine;

/// <summary>
/// Utility methods for getting random values. Why make decisions when I could just not instead?
/// </summary>
public static class RandomUtil {
    public static bool RandomBool() {
        return (Random.Range(0, 2) == 0);
    }
}
