﻿using UnityEngine;
using static MainController;

/// <summary>
/// Handles transitioning between bread position states based on arm movements
/// </summary>
public class TransitionTrigger : MonoBehaviour {
    public MainController Controller;
    public EatingPositionTransition transitionToMake;

    private void OnTriggerEnter2D(Collider2D other) {
        if (transitionToMake != EatingPositionTransition.ToUp
                    && transitionToMake != EatingPositionTransition.ToDown)
            return;

        if (other.tag == "Bread") {
            Controller.TriggerTransition(transitionToMake);
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (transitionToMake != EatingPositionTransition.DownToNone
                    && transitionToMake != EatingPositionTransition.UpToNone)
            return;

        if (Controller.CurrentState == EatingPositionState.None)
            return;

        if (other.tag == "Bread") {
            Controller.TriggerTransition(transitionToMake);
        }
    }
}
